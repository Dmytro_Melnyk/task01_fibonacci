package com.melnyk.services;

import java.util.Scanner;

/**
 * In Fibonacci class user can the size of set and after that program builds
 * Fibonacci numbers and print the biggest odd number and the biggest even
 * number. Also program prints percentage of odd and even Fibonacci numbers.
 */

public final class Fibonacci {

  /**
   * Constructor.
   */
  public Fibonacci() {
  }

  /**
   * enter the size of set and build Fibonacci numbers.
   *
   * @param start - start of interval
   * @param end - end of interval
   */
  public void fibonacci(final int start, final int end) {
    Calculator numbers = new Calculator();

    Scanner size = new Scanner(System.in);

    System.out.println("F1 will be the biggest odd number and "
        + "F2 – the biggest even number");
    System.out.print("Enter the size of set: ");

    final int sizeOfSet = size.nextInt();
    int[] fibonacciNumbers = new int[sizeOfSet];

    fibonacciNumbers[0] = getTheBiggestOdd(
        numbers.getOddNumbers(start, end));
    fibonacciNumbers[1] = getTheBiggestEven(
        numbers.getEvenNumbers(start, end));

    for (int i = 2; i < fibonacciNumbers.length; i++) {
      fibonacciNumbers[i] = fibonacciNumbers[i - 2] + fibonacciNumbers[i - 1];
    }

    for (int number : fibonacciNumbers) {
      System.out.print(number + " ");
    }

    System.out.println("\nPercentage of odd: "
        + (getPercentageOfOdd(fibonacciNumbers)) + "%");
    System.out.println("Percentage of even: "
        + (getPercentageOfEven(fibonacciNumbers)) + "%");
  }

  // get the biggest odd number
  private int getTheBiggestOdd(final int[] fibonacciNumbers) {
    int theBiggestOdd = 0;

    for (int number : fibonacciNumbers) {
      if (number > theBiggestOdd && number % 2 == 1) {
        theBiggestOdd = number;
      }
    }

    return theBiggestOdd;
  }

  // get the biggest even number
  private int getTheBiggestEven(final int[] fibonacciNumbers) {
    int theBiggestEven = 0;

    for (int number : fibonacciNumbers) {
      if (number > theBiggestEven && number % 2 == 0) {
        theBiggestEven = number;
      }
    }

    return theBiggestEven;
  }

  // get percentage of odd number
  private float getPercentageOfOdd(final int[] fibonacciNumbers) {
    float percentage;
    int countOfOddNumbers = 0;

    for (int number : fibonacciNumbers) {
      if (number % 2 == 1) {
        countOfOddNumbers++;
      }
    }

    percentage = (float) countOfOddNumbers * 100 / fibonacciNumbers.length;

    return percentage;
  }

  // get percentage of even number
  private float getPercentageOfEven(final int[] fibonacciNumbers) {
    return 100 - getPercentageOfOdd(fibonacciNumbers);
  }
}
