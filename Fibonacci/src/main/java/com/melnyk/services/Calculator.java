package com.melnyk.services;

import com.melnyk.exceptions.FileChecker;
import com.melnyk.exceptions.FileNotClosedException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * In Calculator class user can enter the interval and after that program prints
 * odd numbers from start to the end and even from end to start. Also find and
 * prints the sum of odd and even numbers.
 */

public final class Calculator {

  /**
   * Constructor.
   */
  public Calculator() {
  }

  /**
   * enter the interval and print odd and even numbers.
   *
   * @param start - start of interval
   * @param end - end of interval
   */
  public void calculate(int start, int end) {

    System.out.println("Odd numbers:");
    for (int number : getOddNumbers(start, end)) {
      System.out.print(number + " ");
    }

    System.out.println("\nEven Numbers:");
    for (int number : getEvenNumbers(start, end)) {
      System.out.print(number + " ");
    }

    System.out.print("\nSum of odd = "
        + getSumOfNumbers(getOddNumbers(start, end)));
    System.out.print("\nSum of even = "
        + getSumOfNumbers(getEvenNumbers(start, end)));
    System.out.println("\n");
  }

  /**
   * get odd numbers.
   *
   * @param start - start of interval
   * @param end- end of interval
   * @return - return array of int
   */
  public int[] getOddNumbers(final int start, final int end) {
    int arraySize = (int) Math.ceil((float) ((end - start) / 2));

    if (start % 2 == 1 || end % 2 == 1) {
      arraySize++;
    }

    int[] arrayOfNumbers = new int[arraySize];

    for (int i = start, j = 0; i <= end; i++) {
      if (i % 2 == 1) {
        arrayOfNumbers[j++] = i;
      }
    }
    return arrayOfNumbers;
  }

  public List<Integer> getOddNumbers(final List<Integer> array) {
    List<Integer> oddNumbers = new ArrayList<>();

    for (int i = 0; i < array.size(); i++) {
      if (array.get(i) % 2 == 1) {
        oddNumbers.add(array.get(i));
      }
    }
    return oddNumbers;
  }

  // get even numbers
  public int[] getEvenNumbers(final int start, final int end) {
    int arraySize = (int) Math.ceil((float) ((end - start) / 2));

    if (start % 2 == 0 || end % 2 == 0) {
      arraySize++;
    }

    int[] arrayOfNumbers = new int[arraySize];

    for (int i = end, j = 0; i >= start; i--) {
      if (i % 2 == 0) {
        arrayOfNumbers[j++] = i;
      }
    }
    return arrayOfNumbers;
  }

  public List<Integer> getEvenNumbers(final List<Integer> array) {
    List<Integer> evenNumbers = new ArrayList<>();
    for (int i = array.size() - 1; i >= 0; i--) {
      if (array.get(i) % 2 == 0) {
        evenNumbers.add(array.get(i));
      }
    }
    return evenNumbers;
  }

  // get sum of numbers
  private int getSumOfNumbers(final int[] arrayOfNumbers) {
    int sunOfNumbers = 0;

    for (int number : arrayOfNumbers) {
      sunOfNumbers += number;
    }
    return sunOfNumbers;
  }

  private int getSumOfNumbers(final List<Integer> arrayOfNumbers) {
    int sunOfNumbers = 0;

    for (int number : arrayOfNumbers) {
      sunOfNumbers += number;
    }
    return sunOfNumbers;
  }

  public int getTheBiggestOddNumberFromFile() {
    return getOddNumbers(getNumbersFromFile())
        .get(getOddNumbers(getNumbersFromFile()).size() - 1);
  }

  public int getTheBiggestEvenNumberFromFile() {
    return getEvenNumbers(getNumbersFromFile())
        .get(0);
  }

  public void numbersFromFile() {
    List<Integer> numbers = getNumbersFromFile();
    System.out.println("Odd numbers from file:");
    for (int num : getOddNumbers(numbers)) {
      System.out.print(num + " ");
    }
    System.out.println("\nEven numbers from file:");
    for (int num : getEvenNumbers(numbers)) {
      System.out.print(num + " ");
    }
    System.out.print("\nSum of odd = "
        + getSumOfNumbers(getOddNumbers(numbers)));
    System.out.print("\nSum of even = "
        + getSumOfNumbers(getEvenNumbers(numbers)));
    System.out.println("\n");
  }

  public List<Integer> getNumbersFromFile() {
    File file = new File(
        "D:\\EPAM\\HomeWork\\task01_fibonacci\\Fibonacci\\arrayNumbers.txt");
    List<Integer> numbers = new ArrayList<>();

    try (BufferedReader reader = new BufferedReader(new FileReader(file));
        FileChecker fileChecker = new FileChecker()) {
      String[] strArray = reader.readLine().split(" ");
      for (String number : strArray) {
        numbers.add(Integer.parseInt(number));
      }
    } catch (IOException | FileNotClosedException e) {
      e.printStackTrace();
    }
    Collections.sort(numbers);
    return numbers;
  }


}
