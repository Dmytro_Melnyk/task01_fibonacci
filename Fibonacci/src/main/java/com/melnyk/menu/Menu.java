package com.melnyk.menu;

import com.melnyk.exceptions.BadEntryException;
import com.melnyk.services.Calculator;
import com.melnyk.services.Fibonacci;
import java.util.Scanner;

/**
 * In Menu class user choose what kind of program he/she want.
 */

public final class Menu {

  /**
   * Constructor.
   */
  private Menu() {
  }

  /**
   * This is the main method which makes use of fibonacci method and calculate
   * method.
   */
  public static void menu() {
    Calculator numbers = new Calculator();
    Fibonacci fibonacciNumbers = new Fibonacci();
    boolean exit = false;
    int start = 0;
    int end = 0;

    Scanner printKey = new Scanner(System.in);
    Scanner interval = new Scanner(System.in);

    do {
      System.out.print("1. Prints odd & even numbers from file.\n"
          + "2. Prints odd & even numbers from the interval.\n"
          + "3. Build Fibonacci numbers from the size of set.\n"
          + "4. Exit.\n"
          + "\nMake your choice: ");

      int key = printKey.nextInt();

      switch (key) {
        case 1:
          numbers.numbersFromFile();
          break;
        case 2:
          System.out.println("Enter the interval: ");
          System.out.print("start = ");
          start = interval.nextInt();

          System.out.print("end = ");
          end = interval.nextInt();

          do {
            try {
              throw new BadEntryException("end less then start, change end: ");
            } catch (BadEntryException e) {
              System.out.println(e);
              end = interval.nextInt();
            }
          } while (start > end);

          numbers.calculate(start, end);
          break;
        case 3:
          if (start != 0 && end != 0) {
            fibonacciNumbers.fibonacci(start, end);
          } else {
            fibonacciNumbers.fibonacci(numbers.getTheBiggestOddNumberFromFile(),
                numbers.getTheBiggestEvenNumberFromFile());
          }
          break;
        case 4:
          exit = true;
          break;
        default:
          break;
      }
    } while (!exit);
  }
}
