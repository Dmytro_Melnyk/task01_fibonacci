package com.melnyk.exceptions;

public class FileNotClosedException extends Exception {

  public FileNotClosedException() {
  }

  public FileNotClosedException(String message) {
    super(message);
  }

  public FileNotClosedException(String message, Throwable cause) {
    super(message, cause);
  }

  public FileNotClosedException(Throwable cause) {
    super(cause);
  }

  public FileNotClosedException(String message, Throwable cause,
      boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
