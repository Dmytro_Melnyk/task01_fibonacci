package com.melnyk.exceptions;

/**
 * @author Dmytro Melnyk
 * @version 1.0
 * @since 2018-11-21
 */
public class FileChecker implements AutoCloseable {

  public FileChecker() {
  }

  @Override
  public void close() throws FileNotClosedException {
    System.out.println("File closed");
  }
}
