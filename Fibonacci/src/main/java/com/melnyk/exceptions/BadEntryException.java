package com.melnyk.exceptions;

public class BadEntryException extends Exception {

  public BadEntryException() {
  }

  public BadEntryException(String message) {
    super(message);
  }

  public BadEntryException(String message, Throwable cause) {
    super(message, cause);
  }

  public BadEntryException(Throwable cause) {
    super(cause);
  }
}
