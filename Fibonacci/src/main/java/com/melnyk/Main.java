/*
Program to EPAM courses.
 */

package com.melnyk;

import com.melnyk.menu.Menu;

/**
 * <h1>Fibonacci numbers</h1>
 * The FibonacciNumbers program implements an application that simply displays:
 * - odd numbers from start to the end of interval and even from end to start; -
 * the sum of odd and even numbers; - Fibonacci numbers; - the biggest odd
 * number and the biggest even number; - percentage of odd and even Fibonacci
 * numbers.
 * <p>
 * There are four classes in this program. The main class is Main, in this class
 * our program starts.
 *
 * @author Dmytro Melnyk
 * @version 1.0
 * @since 2018-11-11
 */

public final class Main {

  /**
   * Constructor.
   */
  private Main() {
  }

  /**
   * The main method.
   *
   * @param args null
   */
  public static void main(final String[] args) {

    Menu.menu();
  }
}
